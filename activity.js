// 1.) Create a new Collection: users

db.users.insertMany([
    {
        firstName: "Nikola",
        lastName: "Tesla",
        age: 64,
        department: "Communication"
    },
    {
        firstName: "Marie",
        lastName: "Curie",
        age: 28,
        department: "Media and Arts"
    },
    {
        firstName: "Charles",
        lastName: "Darwin",
        age: 51,
        department: "Human Resources"
    },
    {
        firstName: "Rene",
        lastName: "Descartes",
        age: 50,
        department: "Media and Arts"
    },
    {
        firstName: "Albert",
        lastName: "Einstein",
        age: 51,
        department: "Human Resources"
    },
    {
        firstName: "Michael",
        lastName: "Faraday",
        age: 30,
        department: "Communication"
    }
])



// 2.) Find users with letter s in their first name OR d in their last name

db.users.find({$or: 
[
    {firstName:{$regex: 's', $options:'$i'}},
    {lastName:{$regex: 's', $options:'$i'}}
]
},
{firstName:1, lastName:1, _id:0}	
)

// 3.) Find users who are from the Human Resources department and their age is greater than or equal to 50.

db.users.find({$and: 
[
    {department:"Human Resources"},
    {age:{$gte:50}}
]
})

//4.) Find users with the letter e in their first name and has an age of less than or equal to 30.

db.users.find({$and: 
[
    {firstName:{$regex: 'e', $options:'$i'}},
    {age:{$lte:30}}
]
})
